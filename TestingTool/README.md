Note: This is not updated to work with Hydra2. Hydra 2 has no GUI. It is purely a CLI. The TestingTool files have been 
left in for possible future extension to the tool.

## TestingTool Django application

### migrations
SQLite database migrations for the web application

### static
Static files, CSS style page and Images required in the frontend

### templates/testmanager
HTML templates for this application


#### TestingTool files
Django classes - views.py, urls.py etc support the Django framework
views.py provides handling of test submission from the frontend

<p align="center">
  <img src="../imgs/HydraProcess.png?raw=true" alt="Hydra Flow Process" width="600" />
</p>
