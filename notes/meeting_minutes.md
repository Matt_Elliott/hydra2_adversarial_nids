## Meeting 1, 8/10/2020

Discussion:

* SSH described the background to the project and expectations.
* SSH demonstrated using Google Scholar search and citation functions.
* Discussed the deliverables and next steps.

Actions:

* ME to make a repo and add minutes folder -> make MD file
* ME to look into setting up Latex
* ME to look up papers on:
    - Adversarial intrusion detection system
    - Robust intrusion detection system
    - Generative adversirial networks
* ME to look at RefWorks or OneNote as a way to reference
* SSH to share the 'How to Read' document
* SSH to share the Hydra/Neptune project repo via OneDrive


SSH Comments (15/10/2020): Adjusted the formating to capture discussion points and action items and to identify contributions. An issues section can also be included, as required.



## Meeting 2, 15/10/2020

Discussion:

* ME explained the background reading is taking time. SSH agreed that it's fine to continue with this for another week.
* SSH explained the importance of identifying the research question(s). This allows us to focus our research. So if a paper is not answering the question(s), leave it. This allows you to keep focused.
* SSH recommended to prepare a project plan to map out the steps in the project.
* ME noted an issue with downloading the Hydra repo. This was resolved during the meeting.

Actions:

* ME to carry on reading about Adversarial intrusion detection system, robust intrusion detection systems and generative adversirial networks as before, also making short notes on useful papers so they can be referred back to at a later stage.
* ME to try to identify the research question(s) to shape the project.
* ME to draft a project plan to identify what to get done by what date, keeping in mind the deadlines.

SSH Comments (15/10/2020): Adjustments as per meeting 1.



## Meeting 3, 23/10/2020

### Progress since last meeting
* Focused on reading up on what already existed with the intention of finding the exact research question. Wasn't able to pinpoint the question.
### Discussions

* SSH: You have a plan for your preliminary research article but what about the entire project?
* ME: I thought you just meant the PRA. I will do the plan for the entire project.
* SSH: What is your progress so far? What have you been doing?
* ME: Trying to get an understanding on the background. Still struggling to find an issue that hasn't been addressed.
Having difficulty managing all the info.
* SSH: There will be a small subset of work that does condense down to the robustness of the NIDS and it being adversary attacked. There will be an enormous amount of work on the wider ML topics.
* SSH: Don't get too bogged down in the fine details of research papers. Just highlight the important parts and move on.
* SSH: Categorise and upload what your background work has been to this date and I can see if you are going in the
correct direction or are missing some important papers.
* SSH: Answered questions 1-7 from PreliminaryResearchArticle/questions.md
* SSH: Plan should include:
  * Background reading
  * Literature review
  * Model / Architecture
  * Implementation
  * Testing
  * Evaluation
  * Analysis



### Action Points
* Categorise and format rough notes on reading that has been done as soon as you can.
* Run Hydra and try it out for yourself.
* Create a plan for the entire project, not just the PRA.
* Focus on defining what the research question is.

### Date of next meeting
2/11/2020

### Recognition
Student initials: ME  
Date: 26/10/2020  

Supervisor initials:  SSH
Date: 30-10-20
Comments: I didn't get a chance to read these minutes before today's meeting. There's a balance to be reached in presenting the discussion, which we could have discussed.
I don't particularly like this conversation-style recording - it's very likely to misquote or selectively quote. I suggest you look at the 8/10 and 15/10 as examples to capture the main discussion points.



## Meeting 4, 30/10/2020

### Progress since last meeting
* Categorised and formatted rough notes on reviewed research papers.
* Wrote some literature reviews for a couple of papers that were read.
* Reviewed the additional papers that SSH sent me.
* Came up with an initial project question:
  * Enhancing robustness of NIDS against evasion attacks through an ensemble of
    adversarially trained ML models using dimensionally-reduced data
* Tried to run Hydra but encountered some issues.

### Discussions
* ME explained he had spent time researching and came up with the project title (above).
* ME explained reasoning as to what the project should focus on and how the title came from the reasoning as follows:
  * Identifying and stopping evasion attacks
  * Using the attack types:
    * DoS
    * U2R
    * R2L
    * Probe
  * Dimensionality reduction
  * Adversarial training of ML models
  * Using the following ML models in an ensemble format:
    * RF
    * SVM
    * KNN
    * MLR
    * NN
* SSH explained there was a lot of work involved with multiple data sets and multiple models that have to be adversely
trained. It could become a very large piece of work.
* SSH explained the benefits of how writing the literature review can help to reduce the scope of the project and
identify what needs to be addressed.
* SSH explained that using and experimenting with Hydra will also help with reducing the scope.
* SSH explained that the ultimate goal of Hydra is to be able to help someone developing a NIDS by simulating it in Hydra
against different types of attacks in order to make it more robust.
* SSH estimated that adding 1 additional ML model (in particular, if a neural network model), creating an ensemble method of decisions from those models and adding
1 extra attack type may be as much as can be achieved in the time available.
* SSH explained the benefits in how making a full project plan for estimating how much work can get done in terms of:
  * What data set you would use.
  * What attack types are in that data set.
  * What model(s) you might use. (e.g. NN).
  * How you will approach DR (Has been done well for DoS but not for other attack types)

### Action Points
* Solve the issues with running Hydra and get familiar with it.
* Write up literature review part of preliminary research article.
* Start making  a project plan with rough estimates of how long each item would take.

### Date of next meeting
6/11/2020

### Recognition
Student initials: ME  
Date: 2/11/2020  

Supervisor initials:  SSH
Date:  04-11-20
Comments: Minutes agreed.


## Meeting 5, 6/11/2020

### Progress since last meeting
ME started to write the literature reviews for a couple of papers.

### Discussions
* ME explained he had a lot of work to do for other modules and struggles to fit
in all the planned project work.
* SSH encouraged ME to keep writing the PRA.
* SSH gave advice on how to put together and categorize the individual paper
reviews. By making and filtering the reviews into  sub-categories, you should be able
to compare and contrast the papers.
* SSH sent the following information to help on writing literature reviews:
  * https://www.rlf.org.uk/resources/what-is-a-literature-review/
  * https://www.qub.ac.uk/directorates/sgc/learning/LearningResources/ResearchSkills/WritingaDissertation/
* SSH made it clear that the PRA is both assessed on the research and the
progress that is made. This means there should be something to talk about in
terms if implementation (i.e. Hydra set up).
* SSH pointed ME towards some time management help:
  * https://www.qub.ac.uk/directorates/sgc/learning/LearningResources/

### Action Points
* Try to get most of the literature reviews done. Aim to complete at least 8
reviews.
* Get hands-on with Hydra.

### Date of next meeting
13/11/2020

### Recognition
Student initials: ME  
Date: 9/11/2020  

Supervisor initials:  SSH
Date:  18-11-20
Comments: The time management resource was provided following a discussion about the importance of allocating sufficient time to the project, which is worth 40 CATS.
It is important to balance study time between the project and other modules.


## Meeting 6, 13/11/2020

### Progress since last meeting
ME made a first draft of the literature review part of the Preliminary Research Article.  
ME self-evaluated his work and noted where improvements can be made.
### Discussions
* SSH had a quick look at the paper during the meeting and notices that a lot of the papers are arXiv.
Recent 2020 papers are acceptable but papers in 2018 that are still arXiv (i.e. not also published in a peer-reviewed conference/journal) might suggest that the work is not
of publishable standard.
* SSH gave an example of an IEEE TNSM paper called LUCID for looking at how literature reviews are presented in terms
of cross referencing and flow.
* SSH said it is worth commenting that this area is quite novel and so there are not a huge amount of
papers specifically on the topic of this project.
* SSH pointed to the references in Lucid about state-of-art deep learning DDoS detection. ME could cite some
papers that study deep learning IDS in the same way.
* ME identified some issues that he's having with setting up Hydra. SSH pointed out the Ubuntu version should be 18.04. SSH also suggested the option of remote access to CSB machines
* if ME's laptop cannot support the VM.

### Action Points
* ME to write an entire first draft of the PRA for next week by EOB on Wednesday 18/11.
* SSH to provide feedback on lit review draft sent 13/11 AM
* SSH to provide feedback on PRA draft by EOB Thursday 19/11.

### Date of next meeting
20/11/2020

### Recognition
Student initials: ME  
Date: 18/11/2020  

Supervisor initials:  SSH
Date:  18/11/20
Comments: Made a couple of edits to the discussion and added some action points. Feedback was provided on the lit review draft on 13/11 PM.



## Meeting 7, 23/11/2020

### Progress since last meeting
ME addressed comments from feedback SSH gave on his PRA drafts.  
ME continued to work on the PRA.
### Discussions
* ME stated had looked and agreed with the feedback SSH game him on previous PRA drafts.
* ME stated he was still working on writing up the PRA but had no real question or concerns.
* SSH said after reviewing a previous draft, she notes that there were some arXiv papers that were referenced. It would
be good to acknowledge this in the report.
* ME said he was planning on giving SSH another updated version before the submission deadline (27/11/2020).
* SSH agreed to review again.
### Action Points
* ME to submit final draft to SSH ASAP.
### Date of next meeting
4/12/2020

### Recognition
Student initials: ME  
Date: 30/11/2020  

Supervisor initials:  SSH
Date:  03-12-20
Comments: Meetings adjusted in this period while ME worked on the PRA. SSH provided feedback on 3 drafts - 13, 20 and 25 November.


## Meeting 8, 4/12/2020

### Progress since last meeting
ME Continued work on 2 other modules which had upcoming deadlines due the following week.
### Discussions
* ME said he was waiting to see the feedback on the PRA that he submitted.
* SSH expected development work to have taken place by this point.
* ME stated he was planning on focusing on the development work for the project over the
xmas break.
* ME also highlighted the fact that he will have less modules in semester 2 and thus more
time to spend on project work.
* ME had no other questions or concerns.  

### Action Points
* ME waiting to see outcome of PRA and will continue work on other modules in the time being.
### Date of next meeting
11/12/2020

### Recognition
Student initials: ME  
Date: 7/12/2020  

Supervisor initials:  SSH
Date:  11-12-2020
Comments: As reflected in the minutes, ME did not get any project work done in the week leading up to this meeting and also reported that no project work would be completed in the following week.
SSH reiterated concerns about lack of progress. There has been no development work on the project during Semester 1, which is a serious concern.
ME suggested that he will be able to focus on it more from Christmas onwards.


## Meeting 9, 11/12/2020

### Progress since last meeting
ME Continued work on 2 other modules which both had submissions due that week.
### Discussions
* ME and SSH went through the feedback on the PRA.
* SSH stated a port scan, an attack that was suggested to be added to the project in the PRA, may get filtered
 out before it gets to an advanced ML stage.
* SSH said to reconsider the old dataset you are planning to use as stated in the PRA. Use a more up to date dataset.
* SSH stated for an attack type, there are a set of features that would be appropriate to perturb.
* SSH suggested perhaps there was a different type of DDoS attack other than SYN flood that could be implemented.
* SSH stated there wasn't a clear conclusion in the literature review in terms of what gap there is and why
it needs to be addressed.
* ME stated he was trying to build a sandbox that would allow for the creation of
NIDS that could have the ability to detect adversarial attacks.
* SSH suggested to map out what is planned. It wasn't clear what was trying to be achieved.
* SSH suggested ME to think about how this benefits and serves the community. Why would somebody use your tool?
* SSH stated the original idea of Hydra was that you could test how robust a NIDS was to adversarial attack.
* SSH suggested to figure out what the contribution will be over and above what already exists in Hydra:  
    * Who is the ultimate user?
    * How would they use it? An experimenting student? An operator looking for a solution?
* SSH suggested it could be a tool to develop understanding of different types of adversarial evasion attacks
on different types of models. For example, they can assess the detection performance gain on having more models in an ensemble
compared to the extra overhead adding more models has.
* ME agreed.
* SSH advised that there is a benefit in representing this as a diagram in terms of how the system will work
and how the user will interact with it.
* ME asked for a final meeting at the end of the semester.
* SSH agreed.


### Action Points
* ME to produce his plans on what he wants to develop moving forward.
### Date of next meeting
18/12/2020

### Recognition
Student initials: ME  
Date: 15/12/2020  

Supervisor initials:  SSH
Date:  18-12-20
Comments: Minutes agreed. A flowchart/pipeline/data flow diagram of the system should be presented for discussion at the next meeting.



## Meeting 10, 18/12/2020

### Progress since last meeting
ME came up with outline as to defining what the main features and milestones will be and why. He outlined how this would
be built into the existing work and what parts would need to be changed.

### Discussions
* ME presented an overview of his plans moving forward:
  * Clarified This tool should be used by students of the network security field who want to easily prototype and then
  assess a NIDS in defence against adversarial attacks.
  * Most of the work will take place in the backend including:
    * Incorporating adversarial training of ML models.
    * Combine the multiple ML models into one ensemble method
    * Add PCA for dimensionality reduction
    * Time depending, adding new attack types and ML models.
  * The current frontend in Hydra will be built upon to allow users to configure their prototype NIDS to incorporate
    the above new features.
* SSH suggested a useful feature would be providing feedback and recommendations to the user on the model that they
created in terms of what they can learn from and improve the model. (Possible ML model that could be added etc...)
* SSH highlighted that due to the fact that only certain features of a packet of a particular attack type can be
perturbed means that the adversarial training cannot be generalised over all features over all attack types. Adversarial
training needs to be specific per attack type using only features that can be perturbed.
* ME expressed difficulty in knowing for sure what features in each attack were suitable for perturbation.
* SSH suggested ME to look at the Keller paper: "Enhancing robustness against adversarial examples in network intrusion
detection systems". It referenced some work that could be useful.
* On the order of implementation, SSH suggested for ME to implement an ensemble method before moving on to adversarial
training of the ML models or applying PCA.
* SSH suggested PCA can be part of the adversarial training as you will want to know what features are influencing the
classification decision.
* SSH suggested when making a plan for the remaining time available, start at the deadline and work back to the present
date. Additionally, complete the work in cycles so that it can be left in a working state.


### Action Points
ME to make plan of what he plans to complete and by what date.
ME to start work on what is planned first

### Date of next meeting
15/1/2021
### Recognition
Student initials: ME
Date: 2/1/2021

Supervisor initials:  SSH
Date:  04-02-21
Comments: Minutes agreed.



## Meeting 11, 15/1/2020

### Progress since last meeting
ME stated he had some very stressful family issus to deal with over the winter break and was simply not able to work.

### Discussions
* ME started off by saying he noticed a problem with Hydra that was not previously detected: while the frontend said a
test was pending, there was nothing running in the background.
* SSH suggested the source of the problem was there could be no test data. She pointed out to ME that there was test
data in the zip files initially provided. That was also a way of creating your own custom test data.
* ME said he received some very stressful news about a family member so was not able to complete any of the planned
work for any module over the christmas period.
* SSH suggested informing the head of year about this issue.
* SSH asked ME what his plan was for the rest of the semester
* ME outlined the following plan:
  * week 1: Have Hydra running a simple ensemble method
  * week 2: Adversarial training for SYN flood
  * week 4: Stacked ensemble method
  * week 6: PCA for SYN flood
  * week 8: Add custom messages as feedback on the performance of the system
  * week 10: Add NN as an extra ML model
  * week 11 onwards:
    * update adversarial training for new attack tyoe
    * update PCS for new attack type

### Action Points
* Get the Hydra system completing tests with test data
* Finish Week 1 work
* Make a start at week 2 work

### Date of next meeting
29/1/2021
### Recognition
Student initials: ME
Date: 26/1/2021

Supervisor initials:  SSH
Date:  04-02-21
Comments: ME outlined his plans for project work this semester. We spent most of the short meeting discussing ME's difficult personal situation.
I have recommended to contact the Advisor of Studies as this situation may impact ME's progress this year. Additional support and an extension to the project deadline may be required.


## Meeting 12, 29/1/2021

### Progress since last meeting
* ME addressed various issues he was having with getting the system running
* (Update: which it now is with the training data provided)
### Discussions
* ME explained there were many small issues that caused him problems but was happy that he had figured it all out.
* SSH explained these issues were completely normal.
* SSH said she would provide the training data.

### Action Points
* Prove ensemble is working
* Finish week 2 work (adversarial training)

### Date of next meeting
12/2/2021
### Recognition
Student initials: ME
Date: 26/1/2021

Supervisor initials:  SSH
Date:  04-02-21
Comments: In addition to the above, we also briefly discussed ME's personal situation. ME advised that he has not yet contacted his AoS but that he plans to. I recommend this.


## Meeting 13, 12/2/2021

### Progress since last meeting
* Ensemble method completed
* ME started to look into how to implement adversarial training of classifiers

### Discussions
* ME Started of by giving a quick demo showing how a user could select a set of classifiers to be combined
together into an ensemble method.
* SSH asked ME if he had any results recorded
* ME said all classifiers on all attacks produced an accuracy of 0.
* SSH said that should not be correct. SSH stated that since some classifiers were able
to get above 0% accuracy, it would be interesting to combine those classifiers together in order to measure the
effectiveness of the ensemble method. Keeping a running record is useful to compare your progress against.
* SSH said to focus on writing up all the different combinations of classifiers to attacks and map to
relevant ensemble literature.
* ME floated the idea of having multiple ways to adversarially train classifiers in order to compare effectiveness.
* SSH advised ME to be cautious on the amount of work required to get a GAN correct.
* ME said he had started to look into the best way to adversarially train the classifiers.
* SSH stated that there were different way to train the model in terms of applying a GAN to the background or
attack traffic. Once the backend training is complete, it can be complemented by some feedback to the user on the
selected attack configuration.  
* ME agreed

### Action Points
* Produce finding on ensemble performance.
* Start adversarial training.

### Date of next meeting
26/02/2021

### Recognition
Student initials: ME
Date: 24/02/2021

Supervisor initials:  SSH
Date:  05/03/21
Comments: Discussion as described. Regarding the running record, the recommendation is to produce a baseline set of results for the system without the ensemble method so that the results from the ensemble can be compared with the baseline.


## Meeting 14, 26/2/2021

### Progress since last meeting
* ME had to spend time debugging the system 

### Discussions
* After running a script that ran every test in every configuration, ME realised adversarial attacks were not being picked up,
and thus the default NIDS attack classification of 0% was being used. ME Went through the whole system and was looking for 
the solution.
* SSH Asked ME what he had tried.
* ME listed the steps he had taken and the ideas he had tried.
* SSH put ME in contact with a student that had used Neptune before as they had to make slight changes to an existing faucet
file in order for their system to work.

### Action Points
* ME to liaise with student with working Neptune.
* ME to carry on with implementing adversarial training.

### Date of next meeting
12/03/2021

### Recognition
Student initials: ME
Date: 03/03/2021

Supervisor initials:  SSH
Date:  05/03/21
Comments: ME reported being stuck on the 0% problem for some time. SSH advised that it's ok to get in touch as soon as a progress blocking problem such as this arises. 
ME also advised that he has been given a 2 week extension on his FYP - to 7th May. 


## Meeting 15, 12/3/2021

### Progress since last meeting
* ME Fixed issues described in previous meeting minutes
* ME produced results of the performance ensemble methods
* ME indentified the [aversarial-robustness-toolbox](https://github.com/Trusted-AI/adversarial-robustness-toolbox) for adversarial training and is in the middle of implementation. 

### Discussions
* ME starting by addressing some questions SSH had raised on the ensemble results ME had produced.
* SSH advised ME to be precise in reporting the exact parameters that were involved in producing the ensemble results.
* SSH stated that James found that the NN could be unreliable. It is best to investigate this. If is is found as unreliable, it would be best to remove it from the ensemble.
* SSH stated that you should start with the baseline SYN flood without any perturbations in order to make a baseline.
* SSH advised when linking the ensemble method results to the literature, have an idea of what you are wanting to find and why. 
* ME discussed he found the aversarial-robustness-toolbox very useful for implementing adversarial training.
* SSH warned ME that he shold be very specific on exactly what form of adversarial training he was using to train the classifiers. Some techniques may not be approptaie or apply to the scope of the project. There were several papers on aversarial training for NIDS that could be useful to look at to see if this toolbox applies.
* ME agreed.

### Action Points
* ME to update existing findings with NN reliability and baseline SYN Flood attack results.
* ME to make sure adversarial training being implemented remains relative to a NIDS system instrad of just applying a range of adversarial training methods to see what works.

### Date of next meeting
26/03/2021

### Recognition
Student initials: ME
Date: 16/03/2021

Supervisor initials:  SSH
Date:  26-03-21
Comments: Good record of meeting. We also discussed and agreed April meetings to account for Easter closure and prep for report submission.


## Meeting 16, 26/3/2021

### Progress since last meeting
* Verified NN are unreliable
* Updated ensemble results to include baseline SYN flood and attack specifiations
* Identified 4 attacks that are included in the literature that models can  be adversarilly trained on.
* Almost completed adversarial trining but encountered some set backs (mentioned below).

### Discussions
* ME startred by telling SSH he had updated ensemble results to include baseline SYN Flood and attack parameters.
* ME then explained that in order to implement adversarial training, the labels of the data had to be one hot encoded. However, this radically changed the performance of almost all classifiers. [See results here ](https://qubstudentcloud-my.sharepoint.com/:w:/r/personal/40153557_ads_qub_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B8EA3F762-9832-4302-BB15-B530434EE59F%7D&file=26_03_2012_meeting_resources.docx&nav=eyJjIjoxMzU0NjU1OTc3fQ&action=default&mobileredirect=true&cid=4b650f21-5b07-45f8-b935-f4ed52c1fd09). SVM was improved to 100% accuracy accross all attack types using one hot encoding. The lables have to be one hot encoded for the [aversarial-robustness-toolbox](https://github.com/Trusted-AI/adversarial-robustness-toolbox) that was being used.
* SSH said ideally the one hot encoded results should be the same as the origional label encoding.
* ME agreed
* SSH suggested the one hot encoded values could simply be a baseline for the adversarial training, and to be used to quanitfy the effect adversarial training had on the classifiers. 
* ME identified the 4 training techniques that he identified from the literature that can be implemented:
    * FGSM
    * BIM
    * Carlini and Wagner L_2
    * Carlini and Wagner L_inf
* ME stated that only classifiers with loss gradients can be adversarily trained with these techniques:
    * RF -> No
    * KNN -> No
    * SVM -> Yes
    * LR -> Yes
* SSH identified from the one [hot encoding results ](https://qubstudentcloud-my.sharepoint.com/:w:/r/personal/40153557_ads_qub_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B8EA3F762-9832-4302-BB15-B530434EE59F%7D&file=26_03_2012_meeting_resources.docx&nav=eyJjIjoxMzU0NjU1OTc3fQ&action=default&mobileredirect=true&cid=4b650f21-5b07-45f8-b935-f4ed52c1fd09), that only LR with with rate, payload and pariflow are not at 100% accuracy. Thus, the gains that can be made from adversarial training are very limited.
* SSH encouraged ME to take another look into the ensemble work. Try to build a more in-depth analysis of what, why and how it compares with the related work. See if there is an extension to the existing work and what that would look like. 
* SSH kindly offered ME to meet on the comming thursday to discuss ensemble plans.
* ME took the offer.

### Action Points
* ME to produce an plan based on SSH reccomendations above for the meeting on thursday.

### Date of next meeting
1/4/2021

### Recognition
Student initials: ME
Date: 29/3/2021

Supervisor initials:  SSH
Date:  16-04-21
Comments: Minutes agreed.


## Meeting 17, 1/4/2021

### Progress since last meeting
* ME produced a plan based on SSH reccomendations from previous meeting (15). [Read plan doc here](https://qubstudentcloud-my.sharepoint.com/:w:/r/personal/40153557_ads_qub_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B4D8B9DF0-7F10-4065-8E79-15F071176B51%7D&file=1_4_2021_meeting_resources.docx&action=default&mobileredirect=true)
* Started implementing a custom version of adversarial training that would work with all classifiers, not just LR and SVM (see meeting 15 mins).

### Discussions
* ME said he went through all the ensemble literature and identified where he wanted to make a contribution [see plan here](https://qubstudentcloud-my.sharepoint.com/:w:/r/personal/40153557_ads_qub_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B4D8B9DF0-7F10-4065-8E79-15F071176B51%7D&file=1_4_2021_meeting_resources.docx&action=default&mobileredirect=true). 
* SSH added that by doing the work above, there are 2 benefits that can be obtained.
    1) You can provide analysis of the findings in the research article.
    2) You can present it as the tool that others can use to analise their solutions in the software report.
* ME said in addition to the ensemble plan, he was able to identify and begin to implement adversarial training for models that that was previously not possible (KNN and RF)
* ME ran through some design considerstions such as:
    1) Should the trainer only produce adversarial attack samples?
    Ans: Either soultion could work but ME claimed it made more sense to stick to only preturbing attack samples as it emulates how Hydra is attacking Neptune.
    2) The classifier trains on a dataset of which 1% is attack traffic. Should this be increased?
    Ans: Yes. The previous results will be regenerated as a different training set is being used.
* SSH advised ME to keep a log of the specific paramaters of test that produce results, along with a specific seed for repeatability (if relevant).
* SSH asked if the OHE labels producing different results can be trusted? How has ME checked this?
* ME said he able to consistently reproduce the same results using the same testing and training data. He looked into why this variation in results could happen but did not see anything of weight. He concluded there was no explicit reason to believe there results that were generated by OHE to be incorrect.
* SSH reccomended looking at the difference in the packets of the catagorial vs OHE label results to see if there was any pattern that could offer an explination. If unable to get an explination, the adversarialy trained models will be based off the OHE results as a comparison instead of the origional lebel encoded results.
* ME raised the point that his current adversarial trainer will perturb all faetures. He asked if it would be advantageous to only preturb features that were specific to a particular attack so that the defence to that attack and be analised.
* SSH sad if possible, it would be reccomended.
* SSH said if there is time, an extra attack could be used to demonstrate how robust the model is under the same adversarial pretubations of different attacks. (For example can it detect a SYN flood and a UDP reflection attack that have both had their payload changed?)
### Action Points
* ME to finishing adversarial trainer and produce results
* ME to investigate OHE issue
* ME to work towards having the coding done for the final presentation, software report and research article
### Date of next meeting
16/4/2021

### Recognition
Student initials: ME
Date: 5/4/2021

Supervisor initials:  SSH
Date:  16-04-21
Comments: Minutes agreed.


## Meeting 18, 16/4/2021
### Progress since last meeting
* See meeting resources for progress results: [16_4_2021_meeting_resources](https://qubstudentcloud-my.sharepoint.com/:w:/r/personal/40153557_ads_qub_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B4CE216D7-B8D2-40E4-849C-1F54E4661EFE%7D&file=Document1.docx&action=default&mobileredirect=true)
* ME fixed OHE issue mentioned in previous meeting 17 minutes.
* ME produced results on feature selection.
* ME produced results on Pre-processing.
* ME changed the adversarial trainer such that it would only preturb relevent features.

### Discussions
* ME described his pre-processing and feature selection results to SSH.
* SSH said it would be good to have a pipleine of the internal workings and where features are applied.
* ME explained his adversarial training technique.
* ME explained his reasons for preturbing flow samples rather than live traffic. In short, _Hydra is designed to create attacks in such a way the the flow samples from that attack would evade detection. When training the classifiers, creating adversarial flow sapmles mitagates the risk of over-fitting the training to each specific attack type to match the live attack or causing pretubations to flow samples that will not evade detection and thus not improve the robustness of the classifier._
* SSH stated either flow samples or packets can manipulated to create preturbed samples. All desicions will have to be qualified and reasoning explained.
* On daversarial training, ME clarified from a previous email that some adversarial attack techniques did not allow for the selection of specific features to preturb. The logic required to get arround this problem was complicated and reliable results were not a certinity.
* SSH advised ME to focus on the attacks that allow for selection of specific features in the flow samples.
* In terms of timeline, SSH advised ME to complete end-to-end results. SSH would like to give ME feedback on the architecture, results tables and graphs.
* SSH encouraged ME to send her and outline of the atricle and software report to recieve feedback the following week. 
* SSH said it was important to describe ansd then justify the results.
* SSH encouraged ME to share as he goes when writing the article and software report.
* ME agreed

### Action points
* ME to complie any outstanding test results.
* ME to start and send draft of software report to SSH by end-of-week.
* ME to start and send draft of article to SSH by end-of-week.

### Date of next meeting
30/04/2021

### Recognition
Student initials: ME
Date: 19/4/2021

Supervisor initials:  SSH
Date:  22-04-21
Comments: Minutes agreed as recorded.



## Meeting 19, 04/05/2021
### Progress since last meeting
* See meeting resources used by ME: [4_5_2021_meeting_resources](https://qubstudentcloud-my.sharepoint.com/:w:/r/personal/40153557_ads_qub_ac_uk/_layouts/15/Doc.aspx?sourcedoc=%7B99530514-6841-4403-8C50-112A8D3C6E9D%7D&file=meeting_min_notes_4_4_2021.docx&action=default&mobileredirect=true)
* ME provided draft of Article (apart from abstract, recommendations and conclusions)
* SSH provided feedback on article
* ME started to amend the article following comments from SSH
### Discussions
* ME went through a small list of comments made by SSH that he wanted clarification on (as see in the meeting resources above).
* SSH clarified all points

### Action points
* ME to submit Atricle and Software Report by end of week.

### Date of next meeting
That's all folks!

### Recognition
Student initials: ME
Date: 11/5/2021

Supervisor initials:  
Date:  
Comments: 
