# Preliminary Research Article Plan

### Timeline for completion of PRA
#### week 1 (30/10/2020)
* Identify the research question(s)
* Condense all relevant research articles into [reccomended format](https://canvas.qub.ac.uk/courses/11374/files/1193497?module_item_id=431871) 
* (rough) Introduction:
  * problem to be studied
  * objectives of study
 
#### week 2 (6/11/2020)
* If needed, continue adding to research notes if new relevant papers are found
* Literature review from notes
* References

#### week 3 (13/11/2020)
* Address comments on literature review
* Introduction
  * potential approach to solve the problem
  * merits of this approach compared to other existing approaches
  * Goals
  * Successes
  
#### week 4 (20/11/2020)
* Work on comments
* title
* abstract
* appendix

#### week 5 (27/11/2020)
* Work on comments from Sandra
* submit

